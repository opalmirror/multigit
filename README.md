## NAME

**multigit** - perform functions in multiple git directories

## SYNOPSIS

    multigit --init [--list]
    multigit [summary]
    multigit [--force] gitk [args]
    multigit [--force] [SOME-GIT-COMMANDS] [args]
    multigit --help

## DESCRIPTION

First, **`multigit --init`** finds all local git repositories under the
current working directory,
and stores this list of directories in a _.multigit_ file.
Then,
later **`multigit`** commands will look for the _.multigit_ file either in
the current working directory or parent directory
(all the way up to the root directory),
and perform the requested operation in each of the directories listed
in the file.

## SYNOPSIS

- **--force**

   - When a git command in a subdirectory returns a non-zero exit code (failure), multigit prints an error and stops. When --force is given, it ignores the error and continues on

- **--init**

   - Look for all directories under the current working directory which contain .git subdirectories.  Store this list for later use, in the file _.multigit_

- **--list**

   - Show a list of all repository directories multigit has found.

- **gitk** [args]

   - Run a **gitk** in each of repository directories

- **desc**

   - Run a git describe in each repository and summarize the results in a table.

- **summary** (this is also the default function, with no arguments)

   - For each of the repository directories, list the directory status:

      - **U** - working files are modified but unstaged (need add+commit)
      - **S** - working files are staged to the index (need commit)
      - **?** - working file area includes untracked files (need removal, ignored, or add+commit)
      - **m** - a merge is in progress (need resolved, then commit)
      - **s** - there are one or more stashes present (need stash apply or pop, then drop)
      - **M** - the current commit is not part of the local master branch (needs merge to master)
      - **-** - the working branch is behind its tracked remote branch (need pull)
      - **+** - the working branch is ahead of its tracked remote branch (need push of current branch)
      - **B** - the current commit is not present in the remote tracking branch (need push of current branch)
      - **O** - the current commit is not present in the remote origin master branch (need merge to master+push)

- **[SOME-GIT-CMDS]** [args]

   - In each of the repository directories, perform the relevant git command.

- **--help**

   - produce a usage message

## FILES

- _.multigit_

   - stores a list of local git repository working directories found when running **multigit --init**

- _../.multigit_, _../../.multigit_, etc.

   - used if _.multigit_ is not found in the current working directory

## SEE ALSO

git(1), gitk(1)

## AUTHOR

James Perkins, james@loowit.net, November 2018

## REPORTING BUGS

Project web site: https://gitlab.com/opalmirror/multigit

## COPYRIGHT

Copyright 2018 James Perkins.
License BSD-3-Clause.
See file COPYING in this software distribution for full details.
